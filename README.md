# Questionnaire tool for otree by Paris Experimental Economics Lab ([LEEP](https://www.parisschoolofeconomics.eu/en/research/pse-research-centers/leep-experimental-economics-lab/))

A tool for creating questionnaires in oTree using excel sheets. See examples in leepquest/leepquest.xlsx

## Requirements
[oTree](http://otree.org/) v5+, [pandas](https://pypi.org/project/pandas/), [openpyxl](https://pypi.org/project/openpyxl/) (`pip install -U otree pandas openpyxl`).
